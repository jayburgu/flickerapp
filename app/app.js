angular.module("flickrapp", []);

angular.module("flickrapp").controller("maincontroller", ['$scope','$http', function($scope, $http) {
  $scope.sortType = 'title';
  $scope.sortOrder = false;
  $scope.searchTitle='';

  $scope.sortAscending = function(){
    $scope.sortOrder = false;
  }

  $scope.sortDescending = function(){
    $scope.sortOrder = true;
  }



  $http({method: 'GET',url: 'https://api.flickr.com/services/rest/?method=flickr.people.getPublicPhotos&api_key=a5e95177da353f58113fd60296e1d250&user_id=24662369@N07&format=json&nojsoncallback=1'
}).then(function (response) {
	if(response.data && response.data.photos && response.data.photos.photo)
	$scope.photosList = response.data.photos.photo;

},function (response) {});
}]);
